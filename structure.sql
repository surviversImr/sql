DROP TABLE Coordonnees CASCADE;
CREATE TABLE Coordonnees (
    id SERIAL,
    rue VARCHAR (30) NOT NULL,
    nRue INTEGER NOT NULL,
    codePostal VARCHAR(5) NOT NULL,
    Ville VARCHAR(40)NOT NULL,
    CONSTRAINT pk_Coord PRIMARY KEY (id)
);
DROP TABLE IF EXISTS Vendeur CASCADE;
CREATE TABLE Vendeur (
    RCS VARCHAR (30),
    entrepriseName VARCHAR (30) NOT NULL,
    responsableFirstName VARCHAR (30) NOT NULL,
    responsableLastName VARCHAR (30) NOT NULL,
    CONSTRAINT pk_vendeur PRIMARY KEY (RCS)
);
DROP TABLE IF EXISTS HabitationVendeur CASCADE;
CREATE TABLE HabitationVendeur(
    idHabitation SERIAL PRIMARY KEY,
    idCoordonnees INTEGER NOT NULL,
    idVendeur VARCHAR (30) NOT NULL,
    CONSTRAINT fk_Coord FOREIGN KEY (idCoordonnees) REFERENCES Coordonnees (id) ON DELETE CASCADE,
    CONSTRAINT fk_Vendeur FOREIGN KEY (idVendeur) REFERENCES Vendeur (RCS)
);
DROP TABLE IF EXISTS ProduitMag CASCADE;
CREATE TABLE ProduitMag (
    id SERIAL PRIMARY KEY,
    CodeBarre VARCHAR (30) NOT NULL,
    name VARCHAR (30) NOT NULL,
    DateLimit DATE NOT NULL,
    photo VARCHAR NOT NULL,
    price NUMERIC NOT NULL,
    CONSTRAINT unique_ProduitMag UNIQUE (CodeBarre, DateLimit)
);
DROP TABLE IF EXISTS StockProduitMag CASCADE;
CREATE TABLE StockProduitMag (
    idProduit INTEGER NOT NULL,
    idVendeur VARCHAR (30) NOT NULL,
    stock INTEGER NOT NULL,
    CONSTRAINT unique_stockProduit UNIQUE (idProduit, idVendeur),
    CONSTRAINT fk_Produit FOREIGN KEY (idProduit) REFERENCES ProduitMag (id),
    CONSTRAINT fk_vendeur FOREIGN KEY (idVendeur) REFERENCES Vendeur (RCS)
);
DROP TABLE IF EXISTS Repas CASCADE;
CREATE TABLE Repas (
    id SERIAL PRIMARY KEY,
    name VARCHAR (30) NOT NULL,
    price NUMERIC NOT NULL
);
DROP TABLE IF EXISTS StockRepas CASCADE;
CREATE TABLE StockRepas (
    idRepas INTEGER NOT NULL,
    idVendeur VARCHAR (30) NOT NULL,
    stock INTEGER NOT NULL,
    CONSTRAINT pk_stockRepas PRIMARY KEY (idRepas, idVendeur),
    CONSTRAINT fk_Repas FOREIGN KEY (idRepas) REFERENCES Repas (id),
    CONSTRAINT fk_vendeur FOREIGN KEY (idVendeur) REFERENCES Vendeur (RCS)
);

DROP TABLE IF EXISTS Facteur CASCADE;
CREATE TABLE Facteur (
    id SERIAL PRIMARY KEY,
    Fname VARCHAR(30) NOT NULL,
    Lname VARCHAR(30) NOT NULL,
    dateEnrolling DATE NOT NULL,
    temperature NUMERIC NOT NULL
);
DROP TABLE IF EXISTS HabitationFacteur CASCADE;
CREATE TABLE HabitationFacteur(
    id SERIAL PRIMARY KEY,
    idCoordonnees INTEGER,
    idFacteur INTEGER UNIQUE,
    CONSTRAINT HabitationFacteur_FK1 FOREIGN KEY(idFacteur) REFERENCES Facteur(id) ON DELETE CASCADE,
    CONSTRAINT HabitationFacteur_FK2 FOREIGN KEY(idCoordonnees) REFERENCES Coordonnees(id) ON DELETE CASCADE
);
DROP TABLE IF EXISTS Client CASCADE;
CREATE TABLE Client (
    id SERIAL PRIMARY KEY,
    Fname VARCHAR(30) NOT NULL,
    Lname VARCHAR(30) NOT NULL,
    pseudo VARCHAR(30) NOT NULL,
    pwd VARCHAR(15) NOT NULL
);
DROP TABLE IF EXISTS HabitationClient CASCADE;
CREATE TABLE HabitationClient(
    id SERIAL PRIMARY KEY,
    idCoordonnees INTEGER,
    idClient INTEGER UNIQUE,
    CONSTRAINT HabitationClient_FK1 FOREIGN KEY(idClient) REFERENCES Client(id) ON DELETE CASCADE,
    CONSTRAINT HabitationClient_FK2 FOREIGN KEY(idCoordonnees) REFERENCES Coordonnees(id) ON DELETE CASCADE
);
-- Client(id, Fname, Lname, user, pwd) 
/*HabitationClient(_idCoordonnees_, _idClient_) avec HabitationClient(idCoordonnees) appartenant à Coordonnees(id)
 et avec HabitationClient(_idClient_) appartenant à Client(id) 
 */

/*
 Etat(id, date, label) 
 la lien entre etat et commande sera un clé etranger dans etat
 */
DROP TABLE IF EXISTS Commande CASCADE;
CREATE TABLE Commande(
    id SERIAL PRIMARY KEY,
    date DATE NOT NULL,
    idClient int NOT NULL,
    idFacteur INTEGER NOT NULL,
    idVendeur VARCHAR (30) NOT NULL,
    total NUMERIC DEFAULT 0,
    CONSTRAINT Commande_FK1 FOREIGN KEY(idClient) REFERENCES Client(id),
    CONSTRAINT Commande_FK2 FOREIGN KEY(idFacteur) REFERENCES Facteur(id),
    CONSTRAINT Commande_FK3 FOREIGN KEY(idVendeur) REFERENCES Vendeur(RCS)
);

DROP TABLE IF EXISTS ContenuRepas CASCADE;
CREATE TABLE ContenuRepas(
    commandeId INTEGER NOT NULL,
    repasId INTEGER NOT NULL,
    quantite INTEGER NOT NULL,
    sousTotal NUMERIC,
    CONSTRAINT pk_ContenuRepas PRIMARY KEY (commandeId, repasId),
    CONSTRAINT fk_commande FOREIGN KEY (commandeId) REFERENCES Commande (id),
    CONSTRAINT fk_repas FOREIGN KEY (repasId) REFERENCES Repas (id)
);

DROP TABLE IF EXISTS ContenuProduitMag CASCADE;
CREATE TABLE ContenuProduitMag(
    commandeId INTEGER NOT NULL,
    produitID INTEGER NOT NULL,
    quantite INTEGER NOT NULL,
    sousTotal NUMERIC,
    CONSTRAINT pk_ContenuProduitMag PRIMARY KEY (commandeId, produitId),
    CONSTRAINT fk_commande FOREIGN KEY (commandeId) REFERENCES Commande (id),
    CONSTRAINT fk_produitMag FOREIGN KEY (produitId) REFERENCES ProduitMag (id)
);

/*
 Commande(id, date) 
 
 la lien entre FACTEUR et commande sera un clé etranger dans COMMANDE 
 
 la lien entre Client et commande sera un clé etranger dans COMMANDE
 la lien entre Vendeur et commande sera un clé etranger dans COMMANDE
 
 CommandeProduit(idCommande, idProduit) avec CommandeProduit(idCommande) appartenant à Commande(id) et 
 avec CommandeProduit(idProduit) appartenant à ProduitMag(id) 
 
 la lien entre Vendeur et commande sera un clé etranger dans etat CommandeRepas(idCommande, idRepas) avec CommandeProduit(idCommande) appartenant à Commande(id) et avec CommandeProduit(idRepas) appartenant à Repas(id)*/
DROP TYPE IF EXISTS labelEnum CASCADE;
CREATE TYPE labelEnum AS ENUM ('valide', 'en transit', 'livre', 'annule');

DROP TABLE IF EXISTS Etat CASCADE;
CREATE TABLE Etat(
    id SERIAL PRIMARY KEY,
    date DATE NOT NULL,
    label labelEnum,
    idCommande INTEGER,
    CONSTRAINT fk_commande FOREIGN KEY (idCommande) REFERENCES Commande (id)
);

