DROP FUNCTION IF EXISTS getIdAddress;
CREATE FUNCTION getIdAddress (IN nRue INTEGER,  IN rue VARCHAR (50), IN CP VARCHAR (5), IN ville VARCHAR (30))
RETURNS TABLE(id INTEGER)
AS $func$
	BEGIN
		RETURN QUERY
		SELECT coordonnees.id
		FROM coordonnees WHERE coordonnees.nRue = $1 AND lower(coordonnees.rue) = lower($2) AND coordonnees.CodePostal = $3 AND lower(coordonnees.ville) = lower($4);

		IF NOT FOUND
		THEN
			INSERT INTO coordonnees (nRue, rue, CodePostal, ville) VALUES ($1, $2, $3, $4);
			RETURN QUERY
				SELECT coordonnees.id
				FROM coordonnees WHERE coordonnees.nRue = $1 AND lower(coordonnees.rue) = lower($2) AND coordonnees.CodePostal = $3 AND lower(coordonnees.ville) = lower($4);
		END IF;
	END
$func$
LANGUAGE PLPGSQL
VOLATILE;

/*

	select getidaddress(1, 'Rue de Ploubezre', '22300', 'Lannion')

*/

DROP FUNCTION IF EXISTS getIdClient;
CREATE FUNCTION getIdClient (IN FirstName VARCHAR (30), IN LastName VARCHAR (30), IN username VARCHAR (30), IN password VARCHAR (30))
RETURNS client.id%TYPE 
	AS $$ SELECT id FROM client WHERE lower(fname) = lower($1) AND lower(lname) = lower($2) AND lower(pseudo) = lower($3) AND pwd = $4; $$
	LANGUAGE SQL
	STABLE;


DROP FUNCTION IF EXISTS InscriptionClient;
CREATE FUNCTION InscriptionClient (IN FirstName VARCHAR (30), IN LastName VARCHAR (30), IN username VARCHAR (30), IN password VARCHAR (30), IN nRue INTEGER,  IN rue VARCHAR (50), IN CodePostal VARCHAR (5), IN ville VARCHAR (30))
RETURNS VOID
AS $$
    INSERT INTO Client (fname, lname, pseudo, pwd) VALUES ($1, $2, $3, $4);
    INSERT INTO HabitationClient (idcoordonnees, idclient) VALUES (getIdAddress($5, $6, $7, $8), getIdClient($1, $2, $3, $4));
$$
LANGUAGE SQL
VOLATILE;

DROP FUNCTION IF EXISTS addClientAddress;
CREATE FUNCTION addClientAddress(IN ClientId INTEGER, IN nRue INTEGER,  IN rue VARCHAR (50), IN CodePostal VARCHAR (5), IN ville VARCHAR (30))
RETURNS VOID
AS $$
	INSERT INTO HabitationClient (idcoordonnees, idclient) VALUES (getIdAddress($2, $3, $4, $5), $1);
$$
LANGUAGE SQL
VOLATILE;

/*

facteur

*/
DROP FUNCTION IF EXISTS getIdFacteur;
CREATE FUNCTION getIdFacteur (IN FirstName VARCHAR (30), IN LastName VARCHAR (30), IN dateEnrolling date )
RETURNS facteur.id%TYPE 
	AS $$ SELECT id FROM Facteur WHERE lower(fname) = lower($1) AND lower(lname) = lower($2) AND dateEnrolling = $3 ; $$
	LANGUAGE SQL
	STABLE;


DROP FUNCTION IF EXISTS InscriptionFacteur;
CREATE FUNCTION InscriptionFacteur (IN FirstName VARCHAR (30), IN LastName VARCHAR (30), IN dateEnrolling date ,IN temperature NUMERIC, IN nRue INTEGER,  IN rue VARCHAR (50), IN CodePostal VARCHAR (5), IN ville VARCHAR (30))
RETURNS VOID
AS $$
    INSERT INTO Facteur (fname, lname, dateEnrolling, temperature) VALUES ($1, $2, $3, $4);
    INSERT INTO HabitationFacteur (idcoordonnees, idFacteur) VALUES (getIdAddress($5, $6, $7, $8), getIdFacteur($1, $2, $3));
$$
LANGUAGE SQL
VOLATILE;


/*

Vendeur

*/
DROP FUNCTION IF EXISTS getIdVendeur;
CREATE FUNCTION getIdVendeur (IN RCS VARCHAR (30), IN EntrepriseName VARCHAR (30), IN responsableFirstName VARCHAR (30), IN responsableLastName VARCHAR (30) )
RETURNS vendeur.RCS%TYPE 
	AS $$ SELECT rcs FROM vendeur WHERE lower(rcs) = lower($1) AND lower(entrepriseName) = lower($2) AND lower(responsableFirstName) = lower($3) AND lower(responsableLastName) = lower($4) ; $$
	LANGUAGE SQL
	STABLE;


DROP FUNCTION IF EXISTS InscriptionVendeur;
CREATE FUNCTION InscriptionVendeur (IN RCS VARCHAR (30),IN EntrepriseName VARCHAR (30),  IN responsableFirstName VARCHAR (30), IN responsableLastName VARCHAR (30) , IN nRue INTEGER,  IN rue VARCHAR (50), IN CodePostal VARCHAR (5), IN ville VARCHAR (30))
RETURNS VOID
AS $$
    INSERT INTO vendeur (RCS, responsableFirstName, responsableLastName) VALUES ($1, $2, $3);
    INSERT INTO HabitationVendeur (idcoordonnees, idVendeur) VALUES (getIdAddress($5, $6, $7, $8), getIdVendeur($1, $2, $3, $4));
$$
LANGUAGE SQL
VOLATILE;



/**
Creation nouvelle commande
*/




/**
rechercher
*/

DROP FUNCTION IF EXISTS rechercherProduit;
CREATE FUNCTION rechercherproduit (IN produit VARCHAR (30))
RETURNS TABLE (
        codebarre VARCHAR(30),
        name VARCHAR(30),
        price NUMERIC,
        entreprisename VARCHAR(30),
		datelimit date
) 
AS $$ 
BEGIN 
    RETURN QUERY
        select  produitmag.id,produitmag.name,produitmag.price,vendeur.entreprisename,produitmag.datelimit
from produitmag inner join stockproduitmag on produitmag.id=stockproduitmag.id
inner join vendeur on vendeur.rcs=stockproduitmag.idvendeur  where lower(produitmag.name) LIKE CONCAT('%', lower(produit), '%');
END;
 $$ 

LANGUAGE 'plpgsql';


/* example d'utilsation 

select * from rechercherproduit('Keb');

*/

/**

*/

DROP FUNCTION IF EXISTS rechercherparProduitName_EntrepriseName;
CREATE FUNCTION rechercherparProduitName_EntrepriseName (IN produit VARCHAR (30),IN Entreprisenamein VARCHAR (30))
RETURNS TABLE (
        id int,
        name VARCHAR(30),
        price NUMERIC,
        entreprisename VARCHAR(30)

) 
AS $$ 
BEGIN 
    RETURN QUERY
        select  produitmag.id,produitmag.name,produitmag.price,vendeur.entreprisename,produitmag.datelimit
from produitmag inner join stockproduitmag on produitmag.id=stockproduitmag.id
inner join vendeur on vendeur.rcs=stockproduitmag.idvendeur  where lower(produitmag.name) LIKE CONCAT('%', lower(produit), '%') and lower(vendeur.entreprisename) LIKE CONCAT('%', lower(Entreprisenamein), '%');
END;
 $$ 

LANGUAGE 'plpgsql';

/**

select * from rechercherparProduitName_EntrepriseName('keb','ben');
**/


/**
rechercher
*/

DROP FUNCTION IF EXISTS rechercherRepas;
CREATE FUNCTION rechercherRepas (IN produit VARCHAR (30))
RETURNS TABLE (
        id int,
        name VARCHAR(30),
        price NUMERIC,
        entreprisename VARCHAR(30)
) 
AS $$ 
BEGIN 
    RETURN QUERY
        select  repas.id,repas.name,repas.price,vendeur.entreprisename
from repas inner join stockrepas on repas.id=stockrepas.idrepas
inner join vendeur on vendeur.rcs=stockrepas.idvendeur  where lower(repas.name) LIKE CONCAT('%', lower(produit), '%');
END;
 $$ 

LANGUAGE 'plpgsql';


/* example d'utilsation 

select * from rechercherRepas('Keb');

*/

/**

*/

DROP FUNCTION IF EXISTS rechercherparRepasName_EntrepriseName;
CREATE FUNCTION rechercherparRepasName_EntrepriseName (IN produit VARCHAR (30),IN Entreprisenamein VARCHAR (30))
RETURNS TABLE (
        id int,
        name VARCHAR(30),
        price NUMERIC,
        entreprisename VARCHAR(30)

) 
AS $$ 
BEGIN 
    RETURN QUERY
    select  repas.id,repas.name,repas.price,vendeur.entreprisename
from repas inner join stockrepas on repas.id=stockrepas.idrepas
inner join vendeur on vendeur.rcs=stockrepas.idvendeur 
where lower(repas.name) LIKE CONCAT('%', lower(produit), '%') and lower(vendeur.entreprisename) LIKE CONCAT('%', lower(Entreprisenamein), '%');
END;
 $$ 

LANGUAGE 'plpgsql';

/**

select * from rechercherparRepasName_EntrepriseName('keb','ben');
**/



DROP FUNCTION IF EXISTS CalcCF_nameEntreprise;
CREATE FUNCTION CalcCF_nameEntreprise (IN Entreprisenamein VARCHAR (30))
RETURNS TABLE (
        Entreprisename VARCHAR(30),
        CF NUMERIC
) 
AS $$ 
BEGIN 
    RETURN QUERY
    SELECT  vendeur.entreprisename,SUM(total)
	FROM vendeur INNER JOIN commande on vendeur.rcs=commande.idvendeur
	WHERE lower(vendeur.Entreprisename) LIKE CONCAT('%', lower(Entreprisenamein), '%')
	GROUP BY (vendeur.entreprisename);
END
 $$ 

LANGUAGE 'plpgsql';


/***

calc cf par RCS
**/

--DROP FUNCTION IF EXISTS CalcCF_RCS;
CREATE OR REPLACE FUNCTION CalcCF_RCS (IN rcsin VARCHAR (30))
RETURNS TABLE (
        Entreprisename VARCHAR(30),
        CF NUMERIC
) 
AS $$ 
BEGIN 
    RETURN QUERY
    select  vendeur.entreprisename,SUM(total)
	FROM vendeur INNER JOIN commande on vendeur.rcs=commande.idvendeur
	WHERE   lower(vendeur.rcs) LIKE CONCAT('%', lower(rcsin), '%')
	GROUP BY (vendeur.entreprisename);
END;
 $$ 

LANGUAGE 'plpgsql';


/****

annuler un commande
sil il est en etat validé pas d'autre

**/

DROP FUNCTION IF EXISTS annulerCommande;
CREATE FUNCTION annulerCommande (IN idcommandein INTEGER)
RETURNS VOID
AS $$
BEGIN
   IF NOT EXists(select * from etat WHERE etat.idcommande=idcommandein and etat.label!='valide')
   then INSERT INTO etat(date,label,idcommande) VALUES(current_date,'annule',idcommandein);
   end if;


end;
$$
LANGUAGE 'plpgsql';


/**

Tracage du facteur 
**/

CREATE OR REPLACE FUNCTION tracage_facteur_clients (IN idin INTEGER)
RETURNS TABLE (
        idClient INTEGER,
		dateLivraison date

) 
AS $$ 
BEGIN 
    RETURN QUERY
    select  client.id,etat.date
	FROM commande INNER JOIN facteur on commande.idFacteur=facteur.id
	INNER JOIN client on client.id=commande.idclient
	inner join etat on commande.id=etat.idcommande
	WHERE  facteur.id=idin and etat.label='livre';
END;
 $$ 

LANGUAGE 'plpgsql';

/****

tracage client facteur 
**/
CREATE OR REPLACE FUNCTION tracage_client_facteurs (IN idin INTEGER)
RETURNS TABLE (
        idFacteur INTEGER,
		dateLivraison date

) 
AS $$ 
BEGIN 
    RETURN QUERY
    select  facteur.id,etat.date
	FROM commande INNER JOIN facteur on commande.idFacteur=facteur.id
	INNER JOIN client on client.id=commande.idclient
	inner join etat on commande.id=etat.idcommande
	WHERE  client.id=idin and etat.label='livre';
END;
 $$ 
LANGUAGE 'plpgsql';
/**

Tracage du facteur vendeurs 
**/

CREATE OR REPLACE FUNCTION tracage_facteur_vendeurs (IN idin INTEGER)
RETURNS TABLE (
        idvendeur VARCHAR(30),
		dateLivraison date

) 
AS $$ 
BEGIN 
    RETURN QUERY
    select  vendeur.rcs,etat.date
	FROM commande INNER JOIN facteur on commande.idFacteur=facteur.id
	INNER JOIN vendeur on vendeur.rcs=commande.idvendeur
	inner join etat on commande.id=etat.idcommande
	WHERE  facteur.id=idin and etat.label='en transit';
END;
 $$ 

LANGUAGE 'plpgsql';

--DROP FUNCTION IF EXISTS CalcCF_RCS;
CREATE OR REPLACE FUNCTION CalcCF_RCS (IN rcsin VARCHAR (30),IN datedeb DATE,IN dateFin DATE)
RETURNS TABLE (
        Entreprisename VARCHAR(30),
        CF NUMERIC
) 
AS $$ 
BEGIN 
    RETURN QUERY
    select  vendeur.entreprisename,SUM(total)
	FROM vendeur INNER JOIN commande on vendeur.rcs=commande.idvendeur
	WHERE   lower(vendeur.rcs) LIKE CONCAT('%', lower(rcsin), '%') and commande.date BETWEEN datedeb and dateFin
	GROUP BY (vendeur.entreprisename);
END;
 $$ 

LANGUAGE 'plpgsql';



CREATE OR REPLACE FUNCTION actualise_prix_commande()
RETURNS TRIGGER
AS $$
DECLARE
	rowRepas contenuRepas%rowtype;
	rowProduit contenuProduitMag%rowtype;
	valeur NUMERIC;
	comID INTEGER;
BEGIN
		IF (TG_OP = 'DELETE')
		THEN comID = OLD.commandeId;
		ELSE comID = NEW.commandeId;
		END IF;
		valeur = 0;
		FOR rowRepas IN
			SELECT * FROM contenuRepas WHERE contenuRepas.commandeId = comID
		LOOP
			valeur = valeur + rowRepas.sousTotal;
		END LOOP;
		FOR rowProduit IN
				SELECT * FROM contenuProduitMag WHERE contenuProduitMag.commandeId = comID
		LOOP
			valeur = valeur + rowProduit.sousTotal;
		END LOOP;
		UPDATE commande SET total = valeur WHERE commande.id = comID;
	RETURN NEW;
END
$$
LANGUAGE 'plpgsql';

CREATE TRIGGER actualise_prix_commande AFTER INSERT OR UPDATE OR DELETE ON contenuRepas
FOR EACH ROW EXECUTE PROCEDURE actualise_prix_commande();


CREATE TRIGGER actualise_prix_commande AFTER INSERT OR UPDATE OR DELETE ON contenuProduitMag
FOR EACH ROW EXECUTE PROCEDURE actualise_prix_commande();

CREATE OR REPLACE FUNCTION actualise_prix_repas()
RETURNS trigger AS
$BODY$
BEGIN
	NEW.sousTotal = NEW.quantite * (select price FROM repas where repas.id = NEW.repasId);
	--PERFORM actualise_prix_commande(NEW.commandeId);
	RETURN NEW;
END
$BODY$
LANGUAGE 'plpgsql';

CREATE TRIGGER actualise_prix_repas BEFORE INSERT OR UPDATE ON contenuRepas
FOR EACH ROW EXECUTE PROCEDURE actualise_prix_repas();

CREATE OR REPLACE FUNCTION actualise_prix_produit()
RETURNS trigger AS
$BODY$
BEGIN
	NEW.sousTotal = NEW.quantite * (select price FROM produitMag where produitMag.id = NEW.produitId);
	--PERFORM actualise_prix_commande(NEW.commandeId);
	RETURN NEW;
END
$BODY$
LANGUAGE 'plpgsql';

CREATE TRIGGER actualise_prix_produit BEFORE INSERT OR UPDATE ON contenuProduitMag
FOR EACH ROW EXECUTE PROCEDURE actualise_prix_produit();


CREATE OR REPLACE FUNCTION getIdLastCommande(IN idClient INTEGER, IN idFacteur INTEGER, IN idVendeur VARCHAR (30))
RETURNS TABLE (
	idCommande INTEGER
) AS
$$
BEGIN
RETURN QUERY
	SELECT id from commande WHERE commande.idClient = $1 AND commande.idFacteur = $2 AND commande.idVendeur = $3 
	AND commande.date = (SELECT max(date) FROM commande WHERE commande.idClient = $1 AND commande.idFacteur = $2 AND commande.idVendeur = $3);
END
$$
LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION createCommande(IN idClient INTEGER, IN idFacteur INTEGER, IN idVendeur VARCHAR (30))
RETURNS VOID AS
$$
BEGIN
	INSERT INTO commande (date, idClient, idFacteur, idVendeur) VALUES (current_date, $1, $2, $3);
	INSERT INTO etat (date, label, idCommande) VALUES (current_date, 'valide', getIdLastCommande($1, $2, $3));
END
$$
LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION addRepasToCommand(IN idCommande INTEGER, IN idRepas INTEGER, IN quantite INTEGER)
RETURNS SETOF contenuRepas AS
$$
BEGIN
RETURN QUERY
	SELECT * from contenuRepas WHERE contenuRepas.commandeId = $1 AND contenuRepas.repasid = $2;
	IF NOT FOUND
	THEN
		INSERT INTO contenuRepas (commandeid, repasid, quantite) VALUES ($1, $2, $3);
	ELSE
		UPDATE contenuRepas SET quantite = contenuRepas.quantite + $3 WHERE contenuRepas.commandeid = $1 AND contenuRepas.repasid = $2;
	END IF;
	UPDATE stockRepas SET stock = stock - $3 WHERE stockRepas.idRepas = $2 AND stockRepas.idVendeur = (SELECT idVendeur FROM commande WHERE commande.id = $1);

END
$$
LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION addProduitMagToCommand(IN idCommande INTEGER, IN idProduitMag INTEGER, IN quantite INTEGER)
RETURNS SETOF contenuProduitMag AS
$$
BEGIN
RETURN QUERY
	SELECT * from contenuProduitMag WHERE contenuProduitMag.commandeId = $1 AND contenuProduitMag.produitid = $2;
	IF NOT FOUND
	THEN
		INSERT INTO contenuProduitMag (commandeid, produitid, quantite) VALUES ($1, $2, $3);
		UPDATE stockRepas SET stock = stock - $3 WHERE stockRepas.produitid = $2 AND stockRepas.idVendeur = (SELECT idVendeur FROM commande WHERE commande.id = $1);
	ELSE
		UPDATE contenuProduitMag SET quantite = contenuProduitMag.quantite + $3 WHERE contenuProduitMag.commandeid = $1 AND contenuProduitMag.produitid = $2;
		UPDATE stockProduitMag SET stock = stock - $3 WHERE stockProduitMag.produitid = $2 AND stockProduitMag.idVendeur = (SELECT idVendeur FROM commande WHERE commande.id = $1);
	END IF;
END
$$
LANGUAGE 'plpgsql';


CREATE OR REPLACE FUNCTION removeRepasFromCommand(IN idCommande INTEGER, IN idRepas INTEGER)
RETURNS VOID AS
$$
DECLARE
	quantity INTEGER;
BEGIN
	quantity = (SELECT quantite FROM contenuRepas WHERE contenuRepas.commandeId = $1 AND contenuRepas.repasid = $2);
	DELETE FROM contenuRepas WHERE contenuRepas.commandeId = $1 AND contenuRepas.repasid = $2;

	UPDATE stockRepas SET stock = stock + quantity WHERE stockRepas.idRepas = $2 AND stockRepas.idVendeur = (SELECT idVendeur FROM commande WHERE commande.id = $1);
END
$$
LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION removeProduitMagFromCommand(IN idCommande INTEGER, IN idProduitMag INTEGER)
RETURNS VOID AS
$$
DECLARE
	quantity INTEGER;
BEGIN
	quantity = (SELECT quantite FROM contenuProduitMag WHERE contenuProduitMag.commandeId = $1 AND contenuProduitMag.produitid = $2);
	DELETE FROM contenuProduitMag WHERE contenuProduitMag.commandeId = $1 AND contenuProduitMag.produitid = $2;

	UPDATE stockProduitMag SET stock = stock + quantity WHERE stockProduitMag.produitid = $2 AND stockProduitMag.idVendeur = (SELECT idVendeur FROM commande WHERE commande.id = $1);
END
$$
LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION setEtatTransit(IN idCommandein INTEGER)
RETURNS VOID AS
$$
BEGIN
	IF EXISTS (select * from etat WHERE etat.idcommande = idcommandein and etat.label = 'valide')
   then 
		IF NOT EXISTS (select * from etat WHERE etat.idcommande = idcommandein and etat.label = 'transit')
		THEN
   			INSERT INTO etat(date,label,idcommande) VALUES(current_date,'transit',idcommandein);
		END IF;
   end if;
END
$$
LANGUAGE 'plpgsql';


CREATE OR REPLACE FUNCTION setEtatLivre(IN idCommandein INTEGER)
RETURNS VOID AS
$$
BEGIN
	IF EXISTS (select * from etat WHERE etat.idcommande = idcommandein and etat.label = 'transit')
   then 
		IF NOT EXISTS (select * from etat WHERE etat.idcommande = idcommandein and etat.label = 'livre')
		THEN
   			INSERT INTO etat(date,label,idcommande) VALUES(current_date,'livre',idcommandein);
		END IF;
   end if;
END
$$
LANGUAGE 'plpgsql';

