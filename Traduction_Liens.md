Coordonnees(_id_, rue, nRue, codePostale)

Vendeur(_RCS_, responsableFirstName, responsableLastName)

HabitationVendeur(_idCoordonnees_, _idVendeur_)
avec HabitationVendeur(idCoordonnees) appartenant à  Coordonnees(id)
et avec HabitationVendeur(idVendeur) appartenant à  Vendeur(RCS)


ProduitMag(_codeBarre_, name, dateLimit, photo, price)

Commande(_id_, client, vendeur, facteur, date, total)

ContenuProduitMag(_commandeId_, _codeBarre_, quantite, sous-total)
avec ContenuProduitMag(commandeId) appartenant à Commande(id)
et avec ContenuProduitMag(codeBarre) appartenant à ProduitMag(codeBarre)

Repas(_id_, name, price)

Commande(_id_, client, vendeur, facteur, date, total)

ContenuRepas(_commandeId_, _repasId_, quantite, sous-total)
avec ContenuRepas(commandeId) appartenant à Commande(id)
et avec ContenuRepas(_repasId_) appartenant à ProduitMag(id)



Vendeur(_RCS_, responsableFirstName, responsableLastName)

Commande(_id_, client, vendeur, facteur, date, total)
avec Commande(vendeur) appartenant à Vendeur(RCS)

Vendeur(_RCS_, responsableFirstName, responsableLastName)

Repas(_id_, name, price)

StockRepas(_idRepas_, _idVendeur_, stock)
avec StockRepas(idRepas) appartenant à Repas(id)
et avec StockRepas(idVendeur) appartenant à Vendeur(RCS)

Vendeur(_RCS_, responsableFirstName, responsableLastName)

ProduitMag(_codeBarre_, name, dateLimit, photo, price)

StockProduitMag(_codeBarre_, _idVendeur_, stock)
avec StockProduitMag(codeBarre) appartenant à ProduitMag(codeBarre)
et avec StockProduitMag(idVendeur) appartenant à Vendeur(RCS)

Facteur(id,Fname,Lname,dateEnrolling,temperature)

HabitationFacteur(_idCoordonnees_,_idFacteur_)
avec HabitationFacteur(idCoordonnees) appartenant à  Coordonnees(id)
et avec HabitationFacteur(_idFacteur_) appartenant à  Facteur(id)
et un cle primaire (_idFacteur_)


Client(id,Fname,Lname,user,pwd)

HabitationClient(_idCoordonnees_,_idClient_)
avec HabitationClient(idCoordonnees) appartenant à  Coordonnees(id)
et avec HabitationClient(_idClient_) appartenant à  Client(id)


Etat(id,date,label,dateEnrolling,temperature)


la lien entre etat et commande sera un clé etranger dans etat 

Commande(id,date)
la lien entre Facteur et commande sera un clé etranger dans etat 

la lien entre Client et commande sera un clé etranger dans etat 

la lien entre Vendeur et commande sera un clé etranger dans etat 
CommandeProduit(idCommande,idProduit)
avec CommandeProduit(idCommande) appartenant à 
 Commande(id)
et avec CommandeProduit(idProduit) appartenant à  ProduitMag(id)

la lien entre Vendeur et commande sera un clé etranger dans etat 
CommandeRepas(idCommande,idRepas)
avec CommandeProduit(idCommande) appartenant à 
 Commande(id)
et avec CommandeProduit(idRepas) appartenant à 
 Repas(id)


